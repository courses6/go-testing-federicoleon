package main

import "gitlab.com/atcheri/udemy-go-tests-federicoleon/api/app"

func main() {
	app.StartApp()
}
