package app

import "gitlab.com/atcheri/udemy-go-tests-federicoleon/api/controllers"

func mapUrls() {
	router.GET("/locations/countries/:country_id", controllers.GetCountry)
}
