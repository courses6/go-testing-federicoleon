package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/services"
)

// GetCountry ...
func GetCountry(ctx *gin.Context) {
	country, err := services.GetCountry(ctx.Param("country_id"))
	if err != nil {
		ctx.JSON(err.Status, err)
		return
	}
	ctx.JSON(http.StatusOK, country)
}
