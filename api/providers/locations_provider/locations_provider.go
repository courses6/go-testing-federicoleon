package locationsprovider

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/app/datasources"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/domain/locations"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/utils/apierrors"
)

const (
	apiCountryURL = "https://api.mercadolibre.com/countries/%s"
	// NotFoundErrorMsg ...
	NotFoundErrorMsg = "Invalid restclient response when trying to get country %s"
	// InvalidErrorMsg ...
	InvalidErrorMsg = "Invalid error response when trying to get country %s"
	// InvalidErrorInterfaceMsg ...
	InvalidErrorInterfaceMsg = "Invalid error interface when trying to get country %s"
	// UnmarshalErrorMsg ...
	UnmarshalErrorMsg = "Error when trying to unmarshal country data for %s"
)

// GetCountryProviderImpl ...
type GetCountryProviderImpl struct {
	client datasources.IClient
}

// NewLocationsProvider ...
func NewLocationsProvider(client datasources.IClient) *GetCountryProviderImpl {
	return &GetCountryProviderImpl{client}
}

// GetCountry is the use case (provider) that gets the data from a country
func (p *GetCountryProviderImpl) GetCountry(id string) (*locations.Country, *apierrors.APIError) {
	response, err := p.client.Get(fmt.Sprintf(apiCountryURL, id))

	if err != nil || response == nil {
		return nil, &apierrors.APIError{
			Status:  http.StatusInternalServerError,
			Message: fmt.Sprintf(NotFoundErrorMsg, id),
		}
	}

	if response.StatusCode > 299 {
		var apiErr apierrors.APIError
		if err := p.bodyToJJSON(response.Body, &apiErr); err != nil {
			return nil, &apierrors.APIError{
				Status:  http.StatusInternalServerError,
				Message: fmt.Sprintf(InvalidErrorInterfaceMsg, id),
			}
		}
		return nil, &apiErr
	}

	var country locations.Country
	if err := p.bodyToJJSON(response.Body, &country); err != nil {
		return nil, &apierrors.APIError{
			Status:  http.StatusInternalServerError,
			Message: fmt.Sprintf(UnmarshalErrorMsg, id),
		}
	}

	return &country, nil
}

func (p *GetCountryProviderImpl) bodyToJJSON(body io.ReadCloser, target interface{}) error {
	// read json http response
	jsonDataFromHTTP, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(jsonDataFromHTTP), target)
	fmt.Println(string(jsonDataFromHTTP))
	fmt.Printf("%+v\n", target)
	fmt.Printf("%+v\n", err)

	if err != nil {
		return err
	}

	return nil

}
