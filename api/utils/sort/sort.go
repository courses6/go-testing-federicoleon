package sort

import (
	"sort"
)

// BubbleSort sorts a slice of integer
func BubbleSort(elements []int) {
	keepWorking := true
	for keepWorking {
		keepWorking = false
		for i := 0; i < len(elements)-1; i++ {
			if elements[i] > elements[i+1] {
				keepWorking = true
				elements[i], elements[i+1] = elements[i+1], elements[i]
			}
		}
	}

}

// Sort using the sort package
func Sort(elements []int) {
	sort.Ints(elements)
}

// CreateElements creates the array of integers
func CreateElements(n int) []int {
	elements := make([]int, n)
	j := 0
	for i := n - 1; i > 0; i-- {
		elements[j] = i
		j++
	}

	return elements
}
