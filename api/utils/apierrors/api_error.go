package apierrors

// APIError is the error sent back to the client on api call error
type APIError struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error"`
}
