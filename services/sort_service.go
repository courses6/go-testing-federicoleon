package services

import (
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/utils/sort"
)

// Threshold holds the number beyond which the golang sort algorithm becomes more performant
const Threshold = 20000

// Sort ...
func Sort(elements []int) {
	if len(elements) <= Threshold {
		sort.BubbleSort(elements)
		return
	}
	sort.Sort(elements)
}
