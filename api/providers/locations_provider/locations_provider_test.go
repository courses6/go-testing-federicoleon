package locationsprovider

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/app/datasources"
)

// MockClient is the mock client
type MockClient struct {
	DoFunc func(url string) (*http.Response, error)
}

// Get is the mock client's `Get` func
func (m *MockClient) Get(url string) (*http.Response, error) {
	return m.DoFunc(url)
}

func TestGetCountryHttpRestClientError(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return nil, errors.New(
			"Error from web server",
		)
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, fmt.Sprintf(NotFoundErrorMsg, "AR"), err.Message)
}

func TestGetCountryHttpRestClientNotfound(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return nil, errors.New(NotFoundErrorMsg)
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, fmt.Sprintf(NotFoundErrorMsg, "AR"), err.Message)
}

func TestGetCountryHttpRestClientValidErrorInterface(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return &http.Response{
			Body:       ioutil.NopCloser(bytes.NewBufferString(`{"message":"Country not found","error":"not_found","status":404,"cause":[]}`)),
			StatusCode: 400,
		}, nil
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusNotFound, err.Status)
	assert.EqualValues(t, "Country not found", err.Message)
}

func TestGetCountryHttpRestClientInvalidErrorInterface(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return &http.Response{
			Body:       ioutil.NopCloser(bytes.NewBufferString(`{"message":"Country not found","error":123,"status":404,"cause":[]}`)),
			StatusCode: 400,
		}, nil
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, fmt.Sprintf(InvalidErrorInterfaceMsg, "AR"), err.Message)
}

func TestGetCountryHttpRestClientSuccessInvalidJsonResponse(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return &http.Response{
			Body:       ioutil.NopCloser(bytes.NewBufferString(`{"id":123,"name":"Argentina","time_zone":"GMT-03:00"}`)),
			StatusCode: http.StatusOK,
		}, nil
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, fmt.Sprintf(UnmarshalErrorMsg, "AR"), err.Message)
}

func TestGetCountryNoError(t *testing.T) {
	// Init
	var mockClient datasources.IClient
	mockClient = &MockClient{DoFunc: func(url string) (*http.Response, error) {
		return &http.Response{
			Body:       ioutil.NopCloser(bytes.NewBufferString(`{"id":"AR","name":"Argentina","time_zone":"GMT-03:00", "states":[{"id":"AR-B","name":"Buenos Aires"},{"id":"AR-C","name":"Capital Federal"},{"id":"AR-K","name":"Catamarca"},{"id":"AR-H","name":"Chaco"},{"id":"AR-U","name":"Chubut"},{"id":"AR-W","name":"Corrientes"},{"id":"AR-X","name":"Córdoba"},{"id":"AR-E","name":"Entre Ríos"},{"id":"AR-P","name":"Formosa"},{"id":"AR-Y","name":"Jujuy"},{"id":"AR-L","name":"La Pampa"},{"id":"AR-F","name":"La Rioja"},{"id":"AR-M","name":"Mendoza"},{"id":"AR-N","name":"Misiones"},{"id":"AR-Q","name":"Neuquén"},{"id":"AR-R","name":"Río Negro"},{"id":"AR-A","name":"Salta"},{"id":"AR-J","name":"San Juan"},{"id":"AR-D","name":"San Luis"},{"id":"AR-Z","name":"Santa Cruz"},{"id":"AR-S","name":"Santa Fe"},{"id":"AR-G","name":"Santiago del Estero"},{"id":"AR-V","name":"Tierra del Fuego"},{"id":"AR-T","name":"Tucumán"}]}`)),
			StatusCode: http.StatusOK,
		}, nil
	}}
	provider := NewLocationsProvider(mockClient)

	// Execution
	country, err := provider.GetCountry("AR")

	// Assertion
	assert.Nil(t, err)
	assert.NotNil(t, country)
	assert.EqualValues(t, "AR", country.ID)
	assert.EqualValues(t, "Argentina", country.Name)
	assert.EqualValues(t, "GMT-03:00", country.Timezone)
	assert.EqualValues(t, 24, len(country.States))
}
