package locations

// Country is the main domain model
type Country struct {
	ID             string         `json:"id"`
	Name           string         `json:"name"`
	Timezone       string         `json:"time_zone"`
	GeoInformation GeoInformation `json:"geo_information"`
	States         []State        `json:"states"`
}

// GeoInformation ...
type GeoInformation struct {
	Location GeoLocation `json:"location"`
}

// GeoLocation ...
type GeoLocation struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longtitude"`
}

// State ...
type State struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
