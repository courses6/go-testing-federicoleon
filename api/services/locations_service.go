package services

import (
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/app/datasources"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/domain/locations"
	locationsprovider "gitlab.com/atcheri/udemy-go-tests-federicoleon/api/providers/locations_provider"
	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/utils/apierrors"
)

var (
	client   *datasources.ClientImpl                   = datasources.NewHTTPClient()
	provider *locationsprovider.GetCountryProviderImpl = locationsprovider.NewLocationsProvider(client)
)

// GetCountry ...
func GetCountry(id string) (*locations.Country, *apierrors.APIError) {
	return provider.GetCountry(id)
}
