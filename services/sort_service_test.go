package services

import (
	"testing"

	"gitlab.com/atcheri/udemy-go-tests-federicoleon/api/utils/sort"
)

func TestSort(t *testing.T) {
	// Init
	elements := []int{7, 2, 0, 6, 7, 9, 2, 5, 6, 3}

	// Execution
	Sort(elements)

	// Assertion
	if elements[0] != 0 {
		t.Error("first element should be 0")
	}

	if elements[len(elements)-1] != 9 {
		t.Error("first element should be 9")
	}

}

func TestSortMoreThanThreshold(t *testing.T) {
	// Init

	elements := sort.CreateElements(Threshold + 1)

	// Execution
	Sort(elements)

	// Assertion
	if elements[0] != 0 {
		t.Error("first element should be 0")
	}

	if elements[len(elements)-1] != Threshold {
		t.Errorf("first element should be %d", Threshold)
	}

}

func BenchmarkSort10k(b *testing.B) {
	// Init
	elements := sort.CreateElements(10000)

	for i := 0; i < b.N; i++ {
		Sort(elements)
	}
}

func BenchmarkSort100k(b *testing.B) {
	// Init
	elements := sort.CreateElements(100000)

	for i := 0; i < b.N; i++ {
		Sort(elements)
	}
}
