package app

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

var (
	// PORT is the port number on which the server starts
	PORT   = "8000"
	router = gin.Default()
)

// StartApp ...
func StartApp() {
	mapUrls()
	if err := router.Run(fmt.Sprintf(":%s", PORT)); err != nil {
		panic(err)
	}
}
