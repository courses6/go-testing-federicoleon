package sort

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func executeSortWithTimeout(cn chan<- bool, sortFn func(elements []int)) func(elements []int) {
	return func(elements []int) {
		go func() {
			sortFn(elements)
			cn <- false
		}()

		go func() {
			time.Sleep(50 * time.Millisecond)
			cn <- true
		}()
	}
}

func TestBubbleSortAscOrder(t *testing.T) {
	// Init
	elements := []int{7, 2, 0, 6, 7, 9, 2, 5, 6, 3}

	// First Assertion
	assert.NotNil(t, elements)
	assert.EqualValues(t, 10, len(elements))
	assert.EqualValues(t, 7, elements[0])
	assert.EqualValues(t, 3, elements[len(elements)-1])

	// Execution
	timeoutChan := make(chan bool, 1)
	defer close(timeoutChan)
	executeSortWithTimeout(timeoutChan, BubbleSort)(elements)

	if <-timeoutChan {
		assert.Fail(t, fmt.Sprintf("BubbleSort test timesout: it took more than %d ms", 500))
		return
	}
	// Assertion
	assert.NotNil(t, elements)
	assert.EqualValues(t, 10, len(elements))
	assert.EqualValues(t, 0, elements[0])
	assert.EqualValues(t, 9, elements[len(elements)-1])

}

func TestSortAscOrder(t *testing.T) {
	// Init
	elements := []int{7, 2, 0, 6, 7, 9, 2, 5, 6, 3}

	// First Assertion
	assert.NotNil(t, elements)
	assert.EqualValues(t, 10, len(elements))
	assert.EqualValues(t, 7, elements[0])
	assert.EqualValues(t, 3, elements[len(elements)-1])

	// Execution
	timeoutChan := make(chan bool, 1)
	defer close(timeoutChan)
	executeSortWithTimeout(timeoutChan, Sort)(elements)

	if <-timeoutChan {
		assert.Fail(t, fmt.Sprintf("BubbleSort test timesout: it took more than %d ms", 500))
		return
	}

	// Assertion
	assert.NotNil(t, elements)
	assert.EqualValues(t, 10, len(elements))
	assert.EqualValues(t, 0, elements[0])
	assert.EqualValues(t, 9, elements[len(elements)-1])
}

func TestCreateElements(t *testing.T) {
	assert.ElementsMatch(t, []int{0, 1, 2, 3, 4}, CreateElements(5))
	assert.ElementsMatch(t, []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, CreateElements(10))
}

func BenchmarkBubbleSort(b *testing.B) {
	// Init
	elements := CreateElements(1000000)

	for i := 0; i < b.N; i++ {
		BubbleSort(elements)
	}
}

func BenchmarkSort(b *testing.B) {
	// Init
	elements := CreateElements(1000000)

	for i := 0; i < b.N; i++ {
		Sort(elements)
	}
}
