package datasources

import (
	"net/http"
)

// IClient ...
type IClient interface {
	Get(string) (*http.Response, error)
}

// ClientImpl ...
type ClientImpl struct {
	client *http.Client
}

// NewHTTPClient ...
func NewHTTPClient() *ClientImpl {
	client := &http.Client{}
	return &ClientImpl{
		client,
	}
}

// Get ...
func (c *ClientImpl) Get(url string) (*http.Response, error) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	return c.client.Do(request)
}
